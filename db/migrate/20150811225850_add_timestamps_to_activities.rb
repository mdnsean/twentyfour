class AddTimestampsToActivities < ActiveRecord::Migration
  def change
    change_table :activities do |t|
      t.timestamps null: false
    end
  end
end
