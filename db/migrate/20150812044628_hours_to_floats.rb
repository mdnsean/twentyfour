class HoursToFloats < ActiveRecord::Migration
  def up
    change_column :activities, :hours, :float
  end
  
  def down
    change_column :activities, :hours, :integer
  end
end
