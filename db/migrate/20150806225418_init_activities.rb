class InitActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :name
      t.string :category
      t.integer :hours
      
      t.integer :day
      t.integer :month
      t.integer :year
      
      t.integer :user_id
    end
    
    add_column :users, :name, :string
  end
end
