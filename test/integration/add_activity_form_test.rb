require "test_helper"

class AddActivityFormTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:sean)
  end
  
  test "add 3x3 activities to Sean" do
    3.times do |m| 
      3.times do |n|
        #:create. # vvv "undefined method activities for Nilclass"
        post activities_path, activity: { name: "Play#{n}", category: "Fun", hours: 3,
                                day: 1, month: 2, year: 2015, user: @user }
      end
    end
    get users_path
    assert_select 'h2', "#{@user.name}'s Activities"
    assert_select 'td', "9 hrs"
    
    get activities_path, id: @user.id, cat: "Fun"
    assert_select 'td', "3 hrs"
    
    get activities_path, id: @user.id, cat: "Fun", name: "Play"
    assert_select 'td', "1 hrs"
  end

end
