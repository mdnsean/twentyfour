require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  def setup
    @user = users(:sean)
  end  
  
  test "index redirects if not logged in" do
    get :index
    assert_redirected_to home_path
  end

  test "index displayed if logged in" do
    sign_in @user
    get :index
    assert_template 'users/index'
    assert_select "th", "Category"
    assert_select "th", "Hours"
  end  
end
