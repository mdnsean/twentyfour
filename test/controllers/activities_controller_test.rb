require 'test_helper'

class ActivitiesControllerTest < ActionController::TestCase

  def setup
    @user = users(:sean)
    @entry = activities(:entry_0)
    @entry1 = activities(:entry_1)
    @entry2 = activities(:entry_2)
  end  
  
  test "index redirects if not logged in" do
    get :index
    assert_redirected_to home_path
  end
  
  test "create activity success" do
    sign_in @user
    assert_difference 'Activity.count' do
      post :create, activity: {name: "a", category: "b", hours: 2,
                    day: 1, month: 1, year: 2015, user_id: @user.id }
    end
    assert_redirected_to users_path
  end
  
  test "destroy action success" do
    sign_in @user
    request.env["HTTP_REFERER"] = activities_path
    assert_difference 'Activity.count', -1 do
      post :destroy, id: ActiveRecord::FixtureSet.identify(@entry)
      #delete :destroy, id: ActiveRecord::FixtureSet.identify(@entry1)
      #delete :destroy, id: ActiveRecord::FixtureSet.identify(@entry2)
    end
  # later, with more complex delete pathing, use assert_redirected_to
  end

end
