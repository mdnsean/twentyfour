require 'test_helper'

class ActivityTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:sean)
    @entry = activities(:entry_0)
  end
  
  test "default is valid" do
    assert @entry.valid?
  end
  
  test "no nil fields" do
    assert_not_nil(@entry.user_id)
    assert_not_nil(@entry.name)
    assert_not_nil(@entry.category)
    assert_not_nil(@entry.hours)
    assert_not_nil(@entry.day)
    assert_not_nil(@entry.month)
    assert_not_nil(@entry.year)
  end
  
  test "date must be Fixnums" do
    assert_instance_of(Fixnum, @entry.day)
    assert_instance_of(Fixnum, @entry.month)
    assert_instance_of(Fixnum, @entry.year)
  end

end
