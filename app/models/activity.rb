class Activity < ActiveRecord::Base
  belongs_to :user

  validates :user_id, presence: true
  validates :name, presence: true, length: {maximum: 20,
                   message: "%{count} characters max" }
  validates :category, presence: true, length: {maximum: 20,
                       too_long: "%{count} characters max" }
  validates :hours, presence: true, numericality: {greater_than: 0,
                    less_than_or_equal_to: 24}
  validates :day, presence: true, numericality: true
  validates :month, presence: true, numericality: true
  validates :year, presence: true, numericality: true
end
