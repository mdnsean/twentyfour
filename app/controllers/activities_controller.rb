class ActivitiesController < ApplicationController
  before_action :user_signed_in?, only: [:create, :destroy]
  before_action :correct_user, only: :destroy
  
  def index
    if !user_signed_in?
      redirect_to home_path
    else
      @user = current_user
      @id = params[:id]
      @cat = params[:cat]
      @name = params[:name]

      @act_cats = Activity.select("category, sum(hours) as total").group("category")
      @act_names = Activity.select("category, name, 
                  sum(hours) as total").where("category = ?", @cat).group("name")
      @act_entries = Activity.where("name = ? AND category = ?", @name, @cat)
    end
  end
  
  def show
  end
  
  def new  
  end
  
  def create
    @activity = current_user.activities.build(activity_params)
    if @activity.save!
      redirect_to :back
      #redirect_to users_path
    end
  end

  def edit
  end
  
  def update
  end
  
  def destroy
    @ids = params[:id].split(/\s*,\s*/)
    @ids.each do |id|
      Activity.find(id.to_i).destroy
    end
    flash[:success] = "Entry #{@id} deleted"
    redirect_to :back
  end
  
  private
    def activity_params
      params.require(:activity).permit(:name, :category, :hours, :day, :month, :year)
    end
    
    def correct_user
      @activity = current_user.activities.find_by(id: params[:id])
      redirect_to users_path if @activity.nil?
    end
end
