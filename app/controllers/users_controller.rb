class UsersController < ApplicationController
# user_signed_in?
# current_user
# user_session
  def index
    # @users = User.all
    if !user_signed_in?
      redirect_to home_path
    else
      @user = current_user
      @id = @user.id
      @act_cats = Activity.select("category, sum(hours) as total").group("category")
    end
  end
end
