## **TwentyFour** is a tool to help you develop and maintain productive habits! ##

* *Optimize* your life by categorizing your priorities (e.g. mind, body, and relationships)

* Hold yourself *accountable* by recording daily investments into those pursuits

* Keep yourself *motivated* by looking back on past progress

## Long-term Vision: ##

Aesthetically appealing web app that transforms user data into charts and other visuals!

Try it:
http://sean-twenty-four.herokuapp.com/